require('marko/node-require');

const express = require('express');


const app = express();
const lasso = require('lasso');
// eslint-disable-next-line import/no-unresolved
const indexPage = require('./pages/index');


lasso.configure({
  plugins: ['lasso-marko'],

});
app.use(require('lasso/middleware').serveStatic());

app.get('/', (req, res) => {
  // return res.send('hello');
  indexPage.render({ title: 'test', count: 0 }, res);
});
app.listen(3000, () => {
  console.log('application online');
  if (process.send) {
    process.send('online');
  }
});
