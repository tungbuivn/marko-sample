const appstate = require('../../appstate');


module.exports = class {
  onMount() {
    if (!appstate.state) {
      appstate.init(this.input);
    }
  }
};
