const appstate = require('../../appstate');

// eslint-disable-next-line linebreak-style
module.exports = class {
//   onCreate() {
//     console.log('create');
//   }

  increment() {
    const i = this.input.count + 1;
    // this.input.count=i;
    appstate.state = { count: i };
    // console.log(this.input,appstate.state)
    // this.forceUpdate();
  }

  onMount() {
    const me = this;
    console.log(appstate.state);
    this.subscribeTo(appstate)
      .on('change', (newState) => {
        me.input = newState;
        // me.forceUpdate();
        // console.log("update")
      });
  }
};
