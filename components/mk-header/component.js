const appstate = require('../../appstate');

module.exports = class {
  onMount() {
    const me = this;
    // me.input=me.input;
    this.subscribeTo(appstate)
      .on('change', (newState) => {
        me.input = newState;
      });
  }
};
