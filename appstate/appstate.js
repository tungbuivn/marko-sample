const EventEmitter = require('events');
// var AppState = function () {
//     this.state = {};
//     var me = this;
//     if (typeof window != "undefined") {
//         window.onpopstate = function (event) {
// eslint-disable-next-line max-len
//             // console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
//             me.setState(event.state);
//         }
//     }
//     this.setState=function(obj) {
//         me.state = Object.assign({}, this.state, obj);
//         me.emit('change', this.state);
//     }
// };
// AppState.prototype = EventEmitter.prototype;
// var rx=require("rxjs");
class AppState extends EventEmitter {
  constructor(...args) {
    super(args);

    const me = this;
    if (typeof window !== 'undefined') {
      window.onpopstate = function (event) {
        me.state = event.state;
      };
    }
  }

  init(value) {
    this._state = value;
  }

  set state(value) {
    // console.log('appstate emit...', this._state, value);
    this._state = Object.assign({}, this._state, value);

    this.emit('change', this._state);
  }

  get state() {
    return this._state;
  }
}
module.exports = AppState;
