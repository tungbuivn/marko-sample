const AppState = require('./appstate');

function getState(obj) {
  obj.appState = obj.appState || new AppState();
  return obj.appState;
}

// if (typeof (window) !== 'undefined') {
//   module.exports = getState(window);
// } else {
//   module.exports = getState(this);
// }


module.exports = getState(this);// new AppState();
