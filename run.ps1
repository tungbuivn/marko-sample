function cc_CreateNetWork {
    docker network create -d bridge --attachable netcc
    
}
function cc_StartRedis(){
    docker run -d --restart=always --network=netcc --name=cache redis
}
function dbg(){
    docker run -it --rm --network=netcc -t cc bash
}
function build(){
    docker build -t cc .
}
function run(){
    docker stop cc
    docker rm cc
    $command="docker run --restart=always -v $($pwd.path)/appstate:/app/appstate -v $($pwd.path)/components:/app/components -v $($pwd.path)/index.js:/app/index.js -v $($pwd.path)/layouts:/app/layouts -v $($pwd.path)/pages:/app/pages -p 3000:3000 --name=cc -t cc"
    Invoke-Expression "& $command"
}

function br(){
    docker rm cc -f
    #docker stop  cc
    build
    run
}
build
run